import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import NavigationBar from '../common/NavigationBar';
import AddLocationButton from './AddLocationButton';
import LocationList from './LocationList';
import LocationForm from './LocationForm';
import { actions } from '../../redux/ducks/index';

export default function LocationPage() {
  const [locations, setLocations] = useState(null);
  const [showForm, setShowForm] = useState(false);

  const token = useSelector(state => state.token);
  const allLocations = useSelector(state => state.locations);

  const dispatch = useDispatch();
  let history = useHistory();

  // Changes what workers show up in the table
  const showFormToggle = () => {
    setShowForm(true);
  }
  const hideFormToggle = () => {
    setShowForm(false);
  }

  useEffect(() => {
    if (!token) {
      history.push('/login');
    } else {
      if (allLocations) {
        setLocations(allLocations);
      } else {
        fetch('http://18.218.104.119:3000/locations/getAll', {
          method: 'GET',
          headers: { 'Authorization': `Bearer ${token}` }
        }).then(response => response.text())
          .then(response => {
            dispatch(actions.changeLocations(JSON.parse(response)));
            setLocations(response);
          });
      }
    }
  }, [token, history, allLocations, dispatch]);

  return (
    <div className="LocationPage">
      <NavigationBar />
      <h3>Locations</h3>
      <div className="header mt-3">
        <AddLocationButton showForm={showFormToggle}/>
      </div>
      <LocationList locations={locations}/>
      <AddLocationButton showForm={showFormToggle}/>
      <LocationForm show={showForm ? "block" : "none"} hideForm={hideFormToggle} locations={locations} setLocations={setLocations}/>
    </div>
  );
}
