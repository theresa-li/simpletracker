import React, { useState } from 'react';
import { useSelector } from 'react-redux';

export default function LocationForm({ show, hideForm, locations, setLocations }) {
  const [location, setLocation] = useState({ 
    id: 0,
    name: ''
  });

  const token = useSelector(state => state.token);

  const handleChange = (event) => {
    setLocation({ ...location, [event.target.name]: event.target.value });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    fetch('http://18.218.104.119:3000/locations/create', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
      body: JSON.stringify(location),
    }).then(response => response.text())
      .then(() => {
        setLocations([ ...locations, location ]);
      });
  }

  return (
    <div className="LocationForm form text-left" style={{display: show}}>
      <div className="text-right">
        <input type="button" onClick={hideForm} value="X" />
      </div>
      <h5>Create Location</h5>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Name:</label>
          <input 
            className="form-control"
            type="text"
            name="name"
            value={location.name}
            onChange={handleChange}
          />
        </div>
        <button className="text-right" type="submit" onClick={hideForm}>Submit</button>
      </form>
    </div>
  );
}
