import React from 'react';

export default function LocationListItem({ location }) {
  return (
    <tr>
      <td>{location.name}</td>
    </tr>
  );
}
