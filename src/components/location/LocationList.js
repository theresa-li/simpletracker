import React from 'react';
import LocationListItem from './LocationListItem';

export default function LocationList({ locations }) {
  return (
    <table className="LocationList table my-3">
      <thead>
        <tr>
          <th scope="col">Location</th>
        </tr>
      </thead>
      <tbody>
        {locations ? locations.map(place => <LocationListItem key={place.id} location={place} />) : <tr></tr>}
      </tbody>
    </table>
  );
}
