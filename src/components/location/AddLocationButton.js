import React from 'react';

export default function AddLocationButton ({ showForm }) {
  return (
    <div className="AddLocationButton text-right">
      <input type="button" value="Add Location" onClick={ showForm } />
    </div>
  );
}
