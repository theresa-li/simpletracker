import React from 'react';

export default function AddWorkerButton ({ showForm }) {
  return (
    <div className="AddWorkerButton text-right">
      <input type="button" value="Add Worker" onClick={showForm}/>
    </div>
  );
}
