import React from 'react';

export default function WorkerListItem ({ worker }) {
  return (
    <tr key={worker.id}>
      <td>{worker.name}</td>
      <td>{worker.age}</td>
      <td>{worker.sex}</td>
    </tr>
  );
}
