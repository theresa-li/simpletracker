import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import NavigationBar from '../common/NavigationBar';
import Search from '../common/Search';
import AddWorkerButton from './AddWorkerButton';
import WorkerList from './WorkerList';
import WorkerForm from './WorkerForm';
import { actions } from '../../redux/ducks/index';

export default function WorkerPage () {
  const [search, setSearch] = useState('');
  const [workers, setWorkers] = useState(null);
  const [showForm, setShowForm] = useState(false);

  const token = useSelector(state => state.token);
  const allWorkers = useSelector(state => state.workers);
  
  const dispatch = useDispatch();
  let history = useHistory();

  // Changes what workers show up in the table
  const handleChange = (event) => {
    setSearch(event.target.value);
    if (event.target.value.length) {
      fetch(`http://18.218.104.119:3000/workers/${event.target.value}`, {
        method: 'GET',
        headers: { 'Authorization': `Bearer ${token}` }
      }).then(response => response.text())
        .then(response => setWorkers(JSON.parse(response)));
    } else {
      setWorkers(allWorkers);
    }
  }

  // The following two functions affect whether the modal is visible or not
  const showFormToggle = () => {
    setShowForm(true);
  }
  const hideFormToggle = () => {
    setShowForm(false);
  }

  useEffect(() => {
    if (!token) {
      history.push('/login');
    } else {
      if (allWorkers) {
        setWorkers(allWorkers);
      } else {
        fetch('http://18.218.104.119:3000/workers/getAll', {
          method: 'GET',
          headers: { 'Authorization': `Bearer ${token}` }
        }).then(response => response.text())
          .then(response => {
            dispatch(actions.changeWorkers(JSON.parse(response)));
            setWorkers(response);
          });
      }
    }    
  }, [token, history, allWorkers, dispatch]);
  
  return (
    <div className="WorkerPage">
      <NavigationBar />
      <h3>Workers</h3>
      <div className="header row no-gutters justify-content-between mt-3">
        <Search search={search} handleChange={handleChange} />
        <AddWorkerButton showForm={showFormToggle}/>
      </div>
      <WorkerList workers={workers}/>
      <AddWorkerButton showForm={showFormToggle}/>
      <WorkerForm show={showForm ? "block" : "none"} hideForm={hideFormToggle} workers={workers} setWorkers={setWorkers}/>
    </div>
  );
}
