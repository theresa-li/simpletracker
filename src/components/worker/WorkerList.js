import React from 'react';
import WorkerListItem from './WorkerListItem';

export default function WorkerList ({ workers }) {
  return (
    <table className="WorkerList table my-3">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Age</th>
          <th scope="col">Gender</th>
        </tr>
      </thead>
      <tbody>
        {workers ? workers.map(person => <WorkerListItem key={person.id} worker={person} />) : <tr></tr>}
      </tbody>
    </table>
  );
}
