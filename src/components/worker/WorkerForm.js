import React, { useState } from 'react';
import { useSelector } from 'react-redux';

export default function WorkerForm ({ show, hideForm, workers, setWorkers }) {
  const [worker, setWorker] = useState({
    id: '',
    name: '',
    password: '',
    age: 18,
    sex: "M",
    admin: false,
  });

  const token = useSelector(state => state.token);

  const handleChange = (event) => {
    setWorker({ ...worker, [event.target.name]: event.target.value });
  }

  const handleNumberChange = (event) => {
    setWorker({ ...worker, [event.target.name]: parseInt(event.target.value, 10) });
  }

  const handleCheckbox = (event) => {
    if (worker.admin) {
      setWorker({ ...worker, [event.target.name]: false });
    } else {
      setWorker({ ...worker, [event.target.name]: true });
    }
  }

  // Adds a new worker to the database
  const handleSubmit = (event) => {
    event.preventDefault();
    fetch('http://18.218.104.119:3000/workers/create', {
      method: 'POST',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
      body: JSON.stringify(worker),
    }).then(response => response.text())
      .then(() =>{
        setWorkers([ ...workers, worker ]);
      });
  }

  return (
    <div className="WorkerForm form text-left" style={{display: show}}>
      <div className="text-right">
        <input type="button" onClick={hideForm} value="X" />
      </div>
      <h5>Create Worker</h5>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>ID Number:</label>
          <input className="form-control" 
            type="text" 
            name="id" 
            value={worker.id} 
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Name:</label>
          <input 
            className="form-control"
            type="text"
            name="name"
            value={worker.name}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Password:</label>
          <input 
            className="form-control"
            type="text"
            name="password"
            value={worker.password}
            onChange={handleChange}/>
        </div>
        <div className="form-group">
          <label>Age:</label>
          <input 
            className="form-control"
            type="number"
            name="age"
            min="18"
            value={worker.age}
            onChange={handleNumberChange}/>
        </div>
        <div className="form-group">
          <label>Gender:</label>
          <select 
            className="form-control" 
            name="sex" 
            value={worker.sex} 
            onChange={handleChange}
          >
            <option value="M">Male</option>
            <option value="F">Female</option>
          </select>
        </div>
        <div className="form-group text-right">
          <label className="pr-1">Admin</label>
          <input 
            type="checkbox"
            name="admin"
            checked={worker.admin} 
            onChange={handleCheckbox}
          />
        </div>
        <button className="text-right" type="submit" onClick={hideForm}>Submit</button>
      </form>
    </div>
  );
}
