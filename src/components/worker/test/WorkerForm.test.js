import React from 'react';
import { render } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../redux/store/index';
import WorkerForm from '../WorkerForm';

describe('Workers Page', () => {
  const wrapper = render(
    <Provider store={store}>
      <WorkerForm />
    </Provider>
  );
  const form = wrapper.find('form');
  const inputs = form.find('input');

  it('contains a button for closing modal', () => {
    expect(wrapper.find('input')[0].attribs.type).toEqual('button');
    expect(wrapper.find('input')[0].attribs.value).toEqual('X');
  });

  it('renders a form', () => {
    expect(form.length).toEqual(1);
  });

  it('contains id, name, password, age, and admin fields', () => {
    expect(inputs.length).toEqual(5);
    expect(inputs[0].attribs.name).toEqual('id');
    expect(inputs[1].attribs.name).toEqual('name');
    expect(inputs[2].attribs.name).toEqual('password');
    expect(inputs[3].attribs.name).toEqual('age');
    expect(inputs[4].attribs.name).toEqual('admin');
  });

  it('has a button', () => {
    expect(wrapper.find('button').length).toEqual(1);
  })
});
