import React from 'react';
import { render } from 'enzyme';
import SearchWorker from '../SearchWorker';

describe('Search Field', () => {
  const wrapper = render(<SearchWorker />);

  it('contains search field', () => {
    expect(wrapper.find('.search').length).toEqual(1);
  });
});
