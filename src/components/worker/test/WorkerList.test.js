import React from 'react';
import { render } from 'enzyme';
import WorkerList from '../WorkerList';

describe('Workers list', () => {
  const wrapper = render(<WorkerList />);
  const tableHead = wrapper.find('thead');
  const headerCell = tableHead.find('th');

  it('has a table', () => {
    expect(wrapper.find('table')._root.length).toEqual(1);
  });

  it('contains three columns', () => {
    expect(headerCell.length).toEqual(3);
  });

  it('contains name, age, and gender', () => {
    expect(headerCell[0].children[0].data).toEqual('Name');
    expect(headerCell[1].children[0].data).toEqual('Age');
    expect(headerCell[2].children[0].data).toEqual('Gender');
  });

  it('contains a table body', () => {
    expect(wrapper.find('tbody').length).toEqual(1);
  });
});
