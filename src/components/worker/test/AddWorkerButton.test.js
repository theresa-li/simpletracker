import React from 'react';
import { render } from 'enzyme';
import AddWorkerButton from '../AddWorkerButton';

describe('Add Worker Button', () => {
  const wrapper = render(<AddWorkerButton />);

  it('has an input button', () => {
    expect(wrapper.find('input')[0].attribs.type).toEqual('button');
  });
});
