import React from 'react';
import { shallow } from 'enzyme';
import WorkerListItem from '../WorkerListItem';

describe('Add Worker Button', () => {
  const worker = {
    id: 1,
    name: 'name',
    age: 18,
    sex: 'F'
  }
  const wrapper = shallow(<WorkerListItem worker={worker}/>);
  const row = wrapper.find('tr');
  const data = row.find('td');

  it('contains a row', () => {
    expect(row.length).toEqual(1);
  });

  it('contains three pieces of data', () => {
    expect(data.length).toEqual(3);
  });
});
