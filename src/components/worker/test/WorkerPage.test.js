import React from 'react';
import { render } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../redux/store/index';
import WorkerPage from '../WorkerPage';

describe('Workers Page', () => {
  const wrapper = render(
    <Provider store={store}>
      <WorkerPage />
    </Provider>
  );

  it('contains a header', () => {
    expect(wrapper.find('.header').length).toEqual(1);
  });
});
