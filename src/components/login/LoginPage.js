import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions } from '../../redux/ducks/index';

export default function LoginPage () {
  const [user, setCredentials] = useState({
    id: '',
    password: ''
  });

  const dispatch = useDispatch();
  let history = useHistory();

  const handleChange = (event) => {
    setCredentials({ ...user, [event.target.name]: event.target.value });
  }

  // Retrieves Bearer Auth token if credentials match
  const handleSubmit = (event) => {
    event.preventDefault();
    fetch('http://18.218.104.119:3000/workers/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(user),
    }).then(response => response.text())
      .then(response => {
        dispatch(actions.addToken(response));
        history.push('/worker');
      });
  }

  return (
    <div className="LoginPage text-left">
      <h3>Log In</h3>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>ID Number:</label>
          <input 
            className="form-control col-sm-3" 
            type="text" 
            name="id" 
            value={user.id} 
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Password:</label>
          <input 
            className="form-control col-sm-3" 
            type="text" 
            name="password" 
            value={user.password} 
            onChange={handleChange}
          />
        </div>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}
