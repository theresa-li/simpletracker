import React from 'react';
import { render } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../redux/store/index';
import LoginPage from '../LoginPage';

describe('Log In Page', () => {
  const wrapper = render(
    <Provider store={store}>
      <LoginPage />
    </Provider>
  );
    
  it('renders a form', () => {
    expect(wrapper.find("form").length).toEqual(1);
  });
  
  it('contains id and password fields', () => {
    const inputs = wrapper.find('input');
    expect(inputs.length).toEqual(2);
    expect(inputs[0].attribs.name).toEqual('id');
    expect(inputs[1].attribs.name).toEqual('password');
  });

  it('has a button', () => {
    expect(wrapper.find("button").length).toEqual(1);
  });
});
