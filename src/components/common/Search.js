import React from 'react';

export default function Search ({ search, handleChange }) {
  return (
    <div className="SearchWorker col-sm-4 text-left">
      <input className="search" type="text" value={search} onChange={handleChange} placeholder="Search by name..."/>
    </div>
  );
}
