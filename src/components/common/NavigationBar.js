import React from 'react';
import { NavLink } from 'react-router-dom';

export default function NavigationBar() {
  return (
    <nav className="NavigationBar pb-3">
      <NavLink to="/worker">Workers</NavLink>
      {' | '}
      <NavLink to="/location">Locations</NavLink>
    </nav>
  )
};
