import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import LoginPage from './components/login/LoginPage';
import WorkerPage from './components/worker/WorkerPage';
import LocationPage from './components/location/LocationPage';

function App() {
  return (
    <div className="App container my-5 p-0">
      <Switch>
        <Route path='/login' component={LoginPage} />
        <Route path='/worker' component={WorkerPage} />
        <Route path='/location' component={LocationPage} />
      </Switch>
    </div>
  );
}

export default App;
