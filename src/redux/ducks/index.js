export const actionTypes = {
  ADD_TOKEN: 'ADD_TOKEN',
  CHANGE_WORKERS: 'CHANGE_WORKERS',
  CHANGE_LOCATIONS: 'CHANGE_LOCATIONS',
};

export const actions = {
  addToken: token => ({ type: actionTypes.ADD_TOKEN, payload: token }),
  changeWorkers: workers => ({ type: actionTypes.CHANGE_WORKERS, payload: workers }),
  changeLocations: locations => ({ type: actionTypes.CHANGE_LOCATIONS, payload: locations }),
};

const initialState = {
  token: null,
  workers: null,
  locations: null,
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TOKEN':
      state.token = action.payload;
      return state;
    case 'CHANGE_WORKERS':
      state.workers = action.payload;
      return state;
    case 'CHANGE_LOCATIONS':
      state.locations = action.payload;
      return state;
    default:
      return state;
  }
};
